- name: Unattended package installation
  shell: export DEBIAN_FRONTEND=noninteractive
  changed_when: false

- name: Install MariaDB
  apt:
    pkg: ['mariadb-server', 'mariadb-client', 'python-mysqldb']
    update_cache: no
    state: present

- name: Add .my.cnf
  template:
    src: my.cnf.j2
    dest: /root/.my.cnf
    owner: root
    group: root
    mode: 0600

- name: Start and enable mysql
  service:
    name: mysql
    state: started
    enabled: yes

- name: Set root Password
  mysql_user:
    name: root
    host: "{{ item }}"
    password: "{{ mysql_root_password }}"
    state: present
  with_items:
    - localhost
    - 127.0.0.1
    - ::1

- name: Reload privilege tables
  command: 'mysql -ne "{{ item }}"'
  with_items:
    - FLUSH PRIVILEGES
  changed_when: False

- name: Remove anonymous users
  command: 'mysql -ne "{{ item }}"'
  with_items:
    - DELETE FROM mysql.user WHERE User=''
  changed_when: False

- name: Disallow root login remotely
  command: 'mysql -ne "{{ item }}"'
  with_items:
    - DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1')
  changed_when: False

- name: Remove test database and access to it
  command: 'mysql -ne "{{ item }}"'
  with_items:
    - DROP DATABASE IF EXISTS test
    - DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%'
  changed_when: False

- name: Reload privilege tables
  command: 'mysql -ne "{{ item }}"'
  with_items:
    - FLUSH PRIVILEGES
  changed_when: False

- name: Create database vmail
  mysql_db:
    name: vmail
    state: present

- name: Create database user vmail
  mysql_user:
    name: vmail
    host: localhost
    password: '{{ mysql_vmail_password }}'
    state: present
    priv: vmail.*:ALL

- name: create tables and set default values in database vmail
  command: 'mysql -ne "{{ item }}"'
  vars:
    user: '{{ firstuser_name.split("@")[0] }}'
    domain: '{{ firstuser_name.split("@")[1] }}'
    password: "{{ firstuser_pass | password_hash('sha512') }}"
  with_items:
    - use vmail; CREATE TABLE IF NOT EXISTS `domains` ( `id` int unsigned NOT NULL AUTO_INCREMENT, `domain` varchar(255) NOT NULL, PRIMARY KEY (`id`), UNIQUE KEY (`domain`) );
    - use vmail; CREATE TABLE IF NOT EXISTS `accounts` ( `id` int unsigned NOT NULL AUTO_INCREMENT, `username` varchar(64) NOT NULL, `domain` varchar(255) NOT NULL, `password` varchar(255) NOT NULL, `quota` int unsigned DEFAULT '0', `enabled` boolean DEFAULT '0', `sendonly` boolean DEFAULT '0', PRIMARY KEY (id), UNIQUE KEY (`username`, `domain`), FOREIGN KEY (`domain`) REFERENCES `domains` (`domain`) );
    - use vmail; CREATE TABLE IF NOT EXISTS `aliases` ( `id` int unsigned NOT NULL AUTO_INCREMENT, `source_username` varchar(64), `source_domain` varchar(255), `destination_username` varchar(64) NOT NULL, `destination_domain` varchar(255) NOT NULL, `enabled` boolean DEFAULT '0', PRIMARY KEY (`id`), UNIQUE KEY (`source_username`, `source_domain`, `destination_username`, `destination_domain`), FOREIGN KEY (`source_domain`) REFERENCES `domains` (`domain`) );
    - use vmail; CREATE TABLE IF NOT EXISTS `tlspolicies` ( `id` int unsigned NOT NULL AUTO_INCREMENT, `domain` varchar(255) NOT NULL, `policy` enum('none', 'may', 'encrypt', 'dane', 'dane-only', 'fingerprint', 'verify', 'secure') NOT NULL, `params` varchar(255), PRIMARY KEY (`id`), UNIQUE KEY (`domain`) );
    - use vmail; CREATE TABLE IF NOT EXISTS `quota` ( `username` varchar(100) NOT NULL, `bytes` bigint(20) NOT NULL DEFAULT '0', `messages` int(11) NOT NULL DEFAULT '0', PRIMARY KEY (`username`) );
    - use vmail; insert into domains (domain) values ('{{ domain }}') ON DUPLICATE KEY UPDATE domain=domain;
    - use vmail; insert into accounts (username, domain, password, quota, enabled, sendonly) values ('{{ user }}', '{{ domain }}', '{SHA512-CRYPT}{{ password }}', 2048, true, false) ON DUPLICATE KEY UPDATE domain=domain;
  changed_when: False
